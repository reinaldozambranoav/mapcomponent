import {
    store
} from 'store';

import { fnStoreData } from "functionsV3/MidStore";

import {
    getItemOnline,
} from "commonapi/ItemApi";

import {
    getItemOffline,
} from 'constants/OfflineConstants';

import { fnCheckInfoDocument } from "./FnCheckInfoDocument";

import {
    getExactSearch,
} from 'constants/UserStationConfiguration';

import { fnSearchItemInOrder } from "./FnSearchItemInOrder";

import uuid from "react-uuid";

import {
    filterAddItemV3,
    setUniqueSpId,
} from "containers/OrderList/OrderListActions";

import { setSnackbar } from 'actions/Snackbar';

import {
    open as openDataDocument
} from 'containers/DataDocument/DataDocumentActions';

import {
    setFilterData,
    setFilterValue,
} from "containers/ItemList/ItemListActions";

import {fnUniqueSpId} from "functions/UniqueId"

export const fnAddItemToOrder = async (value, canSearchName = false, isPriceVisor = false, qty, isPriceScreen = false, isMobile = false) => {
    const {
        language, currentCustomer, currentStorage, agroupLines,
        stationConnectionType, defaultQty, configLabel,
        currentPrice, isItemAdded, 
    } = fnStoreData(store);
    let responseItem, responseDocument, responseByLabel,
        searchType = 0, onlineSearchType =  stationConnectionType === 0 ? 0 : (!canSearchName || getExactSearch()) ? 0 : 1
    try {
        if(!isItemAdded){
            try {
                const newUniqueSpId = fnUniqueSpId()
                if(newUniqueSpId === ""){
                    return store.dispatch(setSnackbar(true, 'warning', 'Error generando firma transaccional, reintente nuevamente, si persiste la falla reinicie el aplicativo'));
                }
                localStorage.setItem('uniqueSpId', newUniqueSpId)
                store.dispatch(setUniqueSpId(newUniqueSpId));
            } catch (error) {
                console.error('uniqueSpId => ', error)
                return store.dispatch(setSnackbar(true, 'warning', 'Error generando firma transaccional, reintente nuevamente, si persiste la falla reinicie el aplicativo'));
            }
        }

        responseItem = stationConnectionType === 0
            ? await getItemOffline(value, searchType, undefined, currentStorage.id, currentCustomer)
            : await getItemOnline(value, onlineSearchType, currentStorage.id, currentCustomer);
        if (responseItem.error === true) {
            if (configLabel?.[0] != null) {
                responseByLabel = fnGetItemByLabel(value, configLabel[0]);
                if (responseByLabel.id !== "") {
                    responseItem = stationConnectionType === 0
                        ? await getItemOffline(responseByLabel.id, searchType, undefined, currentStorage.id, currentCustomer)
                        : await getItemOnline(responseByLabel.id, searchType, currentStorage.id, currentCustomer)
                }
            }
            if (responseItem.error === true) {
                searchType = (!canSearchName || getExactSearch()) ? 0 : 1;
                if (searchType === 1) {
                    responseItem = stationConnectionType === 0
                        ? await getItemOffline(value, 1, undefined, currentStorage.id, currentCustomer)
                        : await getItemOnline(value, 1, currentStorage.id, currentCustomer)
                }
                if (responseItem.error === true) {
                    return store.dispatch(setSnackbar(true, 'warning', responseItem.message));
                }
            }
        }

        if (responseItem.data?.length === 1 && !isMobile) {
            responseItem.data = responseItem.data[0]
        }else if(responseItem.data?.length > 1 && !isMobile){
            if(responseItem.data.some(x => x.id === value)){
                responseItem.data = responseItem.data.find(x => x.id === value)
            }
        }

        if ((responseItem.data?.length > 0 && (onlineSearchType === 1 || searchType === 1) && !isPriceScreen) || isMobile) {
            return store.dispatch(setFilterData(responseItem.data));
        } else if (responseItem.data?.length === 0 && !isPriceScreen) {
            return store.dispatch(setSnackbar(true, 'warning', 'Item no encontrado'));
        }
        if(!isPriceScreen){
            responseDocument = fnCheckInfoDocument();
            if (responseDocument.error === true) {
                store.dispatch(setSnackbar(true, 'warning', responseDocument.message));
                return store.dispatch(openDataDocument())
            }
        }

        let newItem = {
            ...responseItem.data,
            qty: responseByLabel?.qty ?? qty ?? defaultQty,
            price: responseByLabel?.price,
            qtyAdded: responseByLabel?.qty ?? qty ?? defaultQty,
        }

        newItem = itemOrderModel(newItem, currentPrice, currentStorage.id);
        if (agroupLines && !isPriceScreen && newItem.isWeight === 0) {
            newItem = {
                ...newItem,
                ...fnSearchItemInOrder(newItem.id, currentStorage.id) ?? newItem,
                qtyAdded: responseByLabel?.qty ?? qty ?? defaultQty,
            }
        };
        if(isPriceScreen){
            return newItem
        }
        store.dispatch(setFilterValue(""));
        return store.dispatch(filterAddItemV3(newItem, isPriceVisor));
    } catch (error) {
        console.error('fnAddItemToOrder => ', error)
        store.dispatch(setSnackbar(true, 'error', "Error no controlado obteniendo productos"));
    }

}

const fnGetItemByLabel = (itemId, config) => {
    const { active, initialDigit, productDigit,
        includeInitialDigitProduct, quantityDigit, quantitydecimal, type } = config
    let currentPos = 0,
        newLenght = 0,
        itemInfo = {
            id: "",
            qty: undefined,
            price: undefined,
        }

    if (!active) return itemInfo;
    // if (initialDigit === parseInt(itemId.substring(currentPos, 1))) {
    //     currentPos++
    //     newLenght = parseInt(currentPos) + parseInt(productDigit);
    //     const currentItemId = itemId.substring(currentPos, newLenght);
    //     console.log('currentItemId => ', currentItemId)
    //     currentPos += parseInt(productDigit)
    //     currentPos = (includeInitialDigitProduct === 0) ? currentPos++ : currentPos
    //     newLenght += parseInt(quantityDigit)

    //     const partInt = itemId.substring(currentPos, newLenght - quantitydecimal)
    //     const partDec = itemId.substring(currentPos + (quantityDigit - quantitydecimal), newLenght)
    //     const partCombin = parseFloat(partInt + '.' + partDec)

    //     console.log('partCombin => ', partCombin)

    //     itemInfo = {
    //         id: currentItemId,
    //         qty: (type === 'C') ? partCombin : undefined,
    //         price: (type === 'P') ? partCombin : undefined
    //     }
    // }
    if (initialDigit === parseInt(itemId.substring(currentPos, 1))) {
        currentPos += (includeInitialDigitProduct === 0) ? 0 : 1
        newLenght = currentPos + parseInt(productDigit);
        const currentItemId = itemId.substring(currentPos, newLenght);
        currentPos += parseInt(productDigit)
        newLenght += parseInt(quantityDigit)
        const partInt = itemId.substring(currentPos, newLenght - quantitydecimal)
        const partDec = itemId.substring(currentPos + (quantityDigit - quantitydecimal), newLenght)
        const partCombin = parseFloat(partInt + '.' + partDec)
        itemInfo = {
            id: currentItemId,
            qty: (type === 'C') ? partCombin : undefined,
            price: (type === 'P') ? partCombin : undefined
        }
    }
    return itemInfo;
}

const fnResponse = (error, message, code) => ({ error, message, code });

const itemOrderModel = (data, currentPrice, storageId) => {
    let priceSet = 0
    switch (+currentPrice) {
        case 1:
            priceSet = data.price1
            break
        case 2:
            priceSet = data.price2
            break
        case 3:
            priceSet = data.price3
            break
        default:
            priceSet = data.price1
            break
    }
    return {
        ...data,
        uniqueKey: uuid(),
        selected: false,
        price: data.price ?? priceSet,
        priceO: priceSet,
        priceMan: 0,
        discount: 0,
        amount: 0,
        taxAmount: 0,
        taxAmountO: 0,
        totalAmount: 0,
        storageId: storageId,
        qtyBo: 0,
        qtyO: 0,
        ulNumber: 0,
        lotNumber: '',
        isUnit: 0,
        taxItf: [],
        typePvp: String(currentPrice),
        oLine: 0,
        oType: null,
        oNumber: null,
        descrip4: '',
        descrip5: '',
        descrip6: '',
        descrip7: '',
        descrip8: '',
        descrip9: '',
        descrip10: '',
        discountPerc: 0,
        idServer: null,
        boxName: '',
        weight: 0,
        volumetricWeight: 0,
        isExist: true,
        isHidden: false,
        internalItemType: itemType(data),
        isModificable: (data.name.indexOf("?") > -1),
        addToCommand: 0,
        hasOffer: false,
        hasAgreement: false,
        storageName: '',
        isDecimal: data?.isDecimal ?? 0,
        lineNumber: -1,
        extraLine: 0,
        without: 0,
        commandDetObservation: "",
        ogTable: 0,
        formula: data.formula == null ? [] : data.formula,
        formulaType: data.formula == null ? -1 : data.formula.length > 0 ? data.formula[0].typeForm : -1,
        discountExist: [],
        unityOrder: data.unity,
        codInst: data.categoryId,
        isServ: data.itemType,
        discountType: -1,
    }
}
const itemType = (data) => {
    const { itemType, isLot, isSer, isComp } = data
    let internalType = 0
    if (itemType === 1) internalType = 1 //Servicio
    else if (isLot === 1) internalType = 2 //Lote
    else if (isSer === 1) internalType = 3 //Seriales
    else if (isComp === 1) internalType = 4 //Compuestos
    return internalType
}