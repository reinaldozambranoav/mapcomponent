
const LOCAL_STORAGE_OFFLINECOMPANY = 'offlineCompany'
export const setOfflineCompany = (license, codSucu) => {
  try {
    const currentInfo = localStorage.getItem(LOCAL_STORAGE_OFFLINECOMPANY);
    let object, encodedString, decodedString;
    if (currentInfo == null) {
      object = [{
        license: license,
        codSucu: codSucu
      }]
      encodedString = Buffer.from(JSON.stringify(object)).toString('base64');
      localStorage.setItem(LOCAL_STORAGE_OFFLINECOMPANY, encodedString);
    } else {
      decodedString = Buffer.from(currentInfo, "base64").toString();
      let jsonInfo = JSON.parse(decodedString);
      if (jsonInfo.findIndex(x => x.license === license && x.codSucu === codSucu) === -1) {
        jsonInfo.push([{
          license: license,
          codSucu: codSucu
        }]);
        encodedString = Buffer.from(JSON.stringify(jsonInfo)).toString('base64');
        localStorage.setItem(LOCAL_STORAGE_OFFLINECOMPANY, encodedString);
      }
    }
  } catch (error) {
    console.error('setOfflineCompany => ', error)
  }
};

export const getIsOfflineCompany = (license, codSucu) => {
  try {
    const currentInfo = localStorage.getItem(LOCAL_STORAGE_OFFLINECOMPANY);
    let decodedString;
    if (currentInfo == null) {
      return false
    } else {
      decodedString = Buffer.from(currentInfo, "base64").toString();
      let jsonInfo = JSON.parse(decodedString);
      if (jsonInfo.findIndex(x => x.license === license && x.codSucu === codSucu) === -1) {
        return false
      }
      return true;
    }
  } catch (error) {
    console.error('getIsOfflineCompany => ', error)
    return false;
  }

}

export const delOfflineCompany = (license, codSucu) => {
  try {
    const currentInfo = localStorage.getItem(LOCAL_STORAGE_OFFLINECOMPANY);
    let decodedString, encodedString;
    if (currentInfo != null) {
      decodedString = Buffer.from(currentInfo, "base64").toString();
      let jsonInfo = JSON.parse(decodedString);
      if (jsonInfo.findIndex(x => x.license === license && x.codSucu === codSucu) > -1) {
        jsonInfo = jsonInfo.filter(x => x.license !== license && x.codSucu !== codSucu);
        if (jsonInfo.length === 0) {
          localStorage.removeItem(LOCAL_STORAGE_OFFLINECOMPANY);
        } else {
          encodedString = Buffer.from(JSON.stringify(jsonInfo)).toString('base64');
          localStorage.setItem(LOCAL_STORAGE_OFFLINECOMPANY, encodedString);
        }
      }
    }
  } catch (error) {
    console.error('delIsOfflineCompany => ', error)
  }
}