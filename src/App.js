import logo from './logo.svg';
import './App.css';
import Tamaps from './components/maps/TaMaps'
import TaMaps from './components/maps/TaMaps';

function App() {
  return (
    <div className="App">
      <TaMaps
        id={'TotalCoinMaps'}
        height={'450px'}
        disabled={false}
        markerContent={'hello'}
        getPolygonList={(data) => console.log(data)}
        getMarker={(data) => console.log(data)}
        getLocation={(data) => console.log(data)}
        isMultiMarker={false}
        geocoder={false} />
    </div>
  );
}

export default App;
