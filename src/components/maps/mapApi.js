import { axios } from "axios"

const fnGetLocationUrl = async (apiRequest) => {
  //SOLO PARA EL USO DE EL ENDPOINT USADO EN REVERSE GEOCODING
  const urlEndpoint = `${apiRequest.endpoint}`
  console.log(`fnGetLocationUrl => ${urlEndpoint}`)
  return await axios.get(urlEndpoint, {
      timeout: 60000,
  })
      .then(response => response)
      .catch(error => handleError(error))
}

const langResponse = (lang, status) => 'No hay conexión contra el servicio, por favor reintente nuevamente'

const handleError = (error, lang = 'ES') => {
  console.error('handleApiError => ', error)
  if (error.toString() === "Error: Network Error") {
      return {
          status: 504,
          data: {
              message: langResponse(lang, 504),
          }
      }
  }
  if (error.response) {
      return error.response
  } else {
      if (error.toString().includes('timeout')) {
          return {
              status: 408,
              data: {
                  message: langResponse(lang, 408),
              }
          }
      }
      return {
          status: 503,
          data: {
              message: langResponse(lang, 503)
          }
      }
  }
}

export const getLocationInfo = (lat, lng) => {
  const apiRequest = {
    endpoint: `https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lng}&format=json`,
  }
  return fnGetLocationUrl(apiRequest)
}

export const getGeocodeInfo = (search, country, limit) => {
  const apiRequest = {
    endpoint: `https://nominatim.openstreetmap.org/?addressdetails=1&q=${search}&countrycodes=${country}&format=json&limit=${limit}&extratags=1`,
  }
  return fnGetLocationUrl(apiRequest)
}

