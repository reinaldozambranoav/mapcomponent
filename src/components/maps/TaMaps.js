import React, { Fragment, memo, useCallback, useEffect, useState } from 'react'
import { MapContainer, TileLayer, } from 'react-leaflet'
// import MapControlsLayer from './components/MapControlsLayer'
import { LocationModel, MarkerModel, polygonModel } from './MapModels'
import MarkerControlLayer from './components/MarkerControlLayer'
// import PolygonControlLayer from './components/PolygonControlLayer'
import { OFF } from './MapConstants'
// import TaGeocoder from './components/TaGeocoder'
import { TaSleep } from '../../functions/Sleep'
import { getGeocodeInfo, getLocationInfo } from './mapApi'
import { isOK } from '../../helper/RestStatus'

const TaMaps = memo(({ id, height, disabled, setLoader,
  markerContent, getPolygonList, getMarker, getLocation,
  isMultiMarker = false, hiddenControls, geocoder }) => {

  const geo = navigator.geolocation
  const [ toggleMarker, setToggleMarker ] = useState(true)
  const [ togglePolygon, setTogglePolygon ] = useState(false)
  const [ coordinates, setCoordinates ] = useState(undefined)
  const [ location, setLocation ] = useState(undefined)
  const [ locations, setLocations ] = useState([])
  const [ polygonList, setPolygonList ] = useState([])
  const [ resultList, setResultList ] = useState([])
  const [ canShowList, setCanShowList ] = useState(false)
  const [ isInitial, setIsInitial ] = useState(true)
  const [ polygonMode, setPolygonMode ] = useState(OFF) // modes: NONE, CREATE, DELETE
  const [ currentPolygon, setCurrentPolygon ] = useState(polygonModel())

  const fnGeocode = async (name, value) => {
    try {
      const response = await getGeocodeInfo(value, 'VE', 20)
      if (isOK(response.status)) {
        const { data } = response
        setResultList(data)
        TaSleep(500)
        setCanShowList((data.length > 0))
      }
    } catch (error) {
      console.error('fnOnChangeSearch => ', error)
    }
  }

  const fnGetLocationInfo = async (coordinates, isMultiMarker = false, isFromResults) => {
    try {
      const response = await getLocationInfo(coordinates.lat, coordinates.lon);
      if (isOK(response.status)) {
        setLocation(LocationModel(response.data, coordinates))
        fnCreateMarker(response.data, isInitial, isFromResults)
      }
    } catch (error) {
      console.error(error)
    }
  }

  const fnOnClickResult = (coords,) => {
    try {
      setCoordinates(coords)
      fnAddMarker(coords, false, true)
    } catch (error) {
      console.error('fnOnClickResult => ', error)
    }
    setResultList([])
    setCanShowList(false)
  }

  const fnAddMarker = (coords, isMultiMarker = false, isFromResults = false) => {
    try {
      if (coords !== undefined) {
        if (locations.find(x => x.coords.lat === coords.lat && x.coords.lon === coords.lon) === undefined) {
          fnGetLocationInfo(coords, false, false, isFromResults)
        }
      } else {
        fnGetInitialPosition()
      }
    } catch (error) {
      console.error('fnAddMarker => ', error)
    }
  }

  const fnCreateMarker = (data, isFromResults = false) => {
    try {
      let markerLocation = LocationModel(data, coordinates)
      if (isInitial) {
        setLocations(preVal => preVal.concat(MarkerModel(preVal.length + 1, markerLocation)))
      } else if (isFromResults) {
        setLocations(preVal => preVal.map((x, i) => {
          if (i === 0) {
            return {
              ...x,
              ...MarkerModel(x.id, markerLocation)
            }
          }
          return x
        }))
      } else {
        setLocations(preVal => preVal.map((x, i) => {
          if (i === 0) {
            return {
              ...x,
              ...MarkerModel(x.id, markerLocation)
            }
          }
          return x
        }))
      }
    } catch (error) {
      console.error('fnCreateMarker => ', error)
    }
  }

  const fnGetInitialPosition = async () => {
    try {
      const options = { timeout: 6000, maximumAge: 0, enableHighAccuracy: true }

      const success = (position) => {
        let coords = position.coords
        setCoordinates({ lat: coords.latitude, lon: coords.longitude })
        setIsInitial(false)
      }

      const error = (error) => console.error('fnGetInitialPosition => ', error)

      if ("geolocation" in navigator) {
        await geo.getCurrentPosition(success, error, options)
      } else {
        console.error('Debe habilitar los servicios de ubicación en el navegador')
      }
    } catch (error) {
      console.error('fnGetInitialPosition => ', error)
    }
  }

  const fnDeleteMarker = (key) => setLocations(preVal => preVal.filter(x => x.uniqueKey !== key))

  useEffect(() => (coordinates === undefined) ? fnGetInitialPosition() : fnGetLocationInfo(coordinates, true, false), [ coordinates ])
  useEffect(() => (polygonList.length === 0) && setPolygonMode(OFF), [ polygonList ])
  useEffect(() => (togglePolygon === false) && setPolygonMode(OFF), [ togglePolygon ])
  useEffect(() => (getPolygonList !== undefined) && getPolygonList(polygonList), [ polygonList ])
  useEffect(() => (getMarker !== undefined) && getMarker(locations), [ locations ])
  useEffect(() => (getLocation !== undefined) && getLocation(location), [ location ])

  return (
    <MapContainer // COMPONENTE TIPO WRAPPER PARA ACCEDER DESDE LOS HIJOS A LOS HOOKS Y CONTEXTO DEL MAPA
      id={id}
      center={[ 6.337, -63.589 ]}
      zoom={5}
      style={{ height: height }}
      scrollWheelZoom={true}
      doubleClickZoom={false}
      zoomControl={false}>
      <TileLayer // CAPA PRINCIPAL DEL COMPONENTE MAPA CREADO POR LEAFLETJS
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
      {(polygonMode === OFF) && < MarkerControlLayer // CAPA DE CONTROL PARA MARCADORES DE UBICACION Y SUCURSALES
          location={location}
          locations={locations}
          toggleMarker={toggleMarker}
          markerContent={markerContent}
          disabled={disabled}
          fnAddMarker={fnAddMarker}
          fnDeleteMarker={fnDeleteMarker}
          coordinates={coordinates}
          geocoder={geocoder} />}
      {/*  */}
      {/* {<PolygonControlLayer // CAPA DE CONTROL DE POLIGONOS
          polygonList={polygonList}
          setPolygonList={setPolygonList}
          togglePolygon={togglePolygon}
          setTogglePolygon={setTogglePolygon}
          disabled={disabled}
          polygonMode={polygonMode}
          currentPolygon={currentPolygon}
          setCurrentPolygon={setCurrentPolygon} />} */}
      {/*  */}
      {/* {(!hiddenControls) && <MapControlsLayer // SEMI CAPA DE CONTROLES, ES SEMI POR QUE ES UNA CAPA SUPERPUESTA CON CSS
          toggleMarker={toggleMarker}
          setToggleMarker={setToggleMarker}
          togglePolygon={togglePolygon}
          setTogglePolygon={setTogglePolygon}
          disabled={disabled}
          polygonMode={polygonMode}
          setPolygonMode={setPolygonMode}
          currentPolygon={currentPolygon}
          setCurrentPolygon={setCurrentPolygon}
          polygonList={polygonList}
          setPolygonList={setPolygonList} />} */}
      {/*  */}
      {/* {(geocoder) && <TaGeocoder
          resultList={resultList}
          canShowList={canShowList}
          fnGeocode={fnGeocode}
          fnOnClickResult={fnOnClickResult} />} */}
    </MapContainer>
  )
})

export default TaMaps
