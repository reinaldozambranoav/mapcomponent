export const fnIsControls = (event) => {
  try {
    let result = false
    if (event !== undefined) {
      let element = event.originalEvent.path.find(x => x.id === 'btnAddMarker' || x.id === 'btnAddPolygon' || x.id === 'btnCancelEdit' || x.id === 'mapControlPanel' || x.id === 'searchBar')
      if (element !== undefined) {
        if (element.id === 'btnAddMarker' || element.id === 'btnAddPolygon' || element.id === 'btnCancelEdit' || element.id === 'mapControlPanel' || element.id === 'searchBar') {
          result = true
        }
      }
    }
    return result
  } catch (error) {
    console.error('fnIsControls => ', error)
  }
}

export const fnIsExactSamePolygon = (polygon, polygonList) => {
  try {
    let result = false
    if (polygon.coords.length !== 0) {
      polygonList.forEach(x => {
        if (x.coords.find(y => polygon.coords.find(z => y.lat === z.lat && y.lng === z.lng)) !== undefined) {
          result = true
        }
      })
    }
    return result
  } catch (error) {
    console.error('fnIsExactSamePolygon => ', error)
  }
}