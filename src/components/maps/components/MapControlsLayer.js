import React, { memo, useState } from 'react'
import { v4 as uuid } from 'uuid'
import AddLocation from '@material-ui/icons/AddLocation'
import GridOn from '@material-ui/icons/GridOn'
import AddIcon from '@material-ui/icons/Add'
import ClearIcon from '@material-ui/icons/Clear'
import SaveIcon from '@material-ui/icons/Save'
import DeleteIcon from '@material-ui/icons/Delete'
import {
  SpeedDial,
  SpeedDialIcon,
  SpeedDialAction,
} from '@material-ui/lab/'
import Filter1Icon from '@material-ui/icons/Filter1'
import Filter2Icon from '@material-ui/icons/Filter2'
import Filter3Icon from '@material-ui/icons/Filter3'
import Filter4Icon from '@material-ui/icons/Filter4'
import Filter5Icon from '@material-ui/icons/Filter5'
import Filter6Icon from '@material-ui/icons/Filter6'
import Filter7Icon from '@material-ui/icons/Filter7'
import Filter8Icon from '@material-ui/icons/Filter8'
import Filter9Icon from '@material-ui/icons/Filter9'
import DoneIcon from '@material-ui/icons/Done'
import { fnIsExactSamePolygon } from '../functions'
import { polygonModel } from '../MapModels'
import { CREATE, DELETE, OFF } from '../MapConstants'

const MapControlsLayer = memo(({ toggleMarker, setToggleMarker, togglePolygon, setTogglePolygon,
  disabled, polygonMode, setPolygonMode, polygonList,
  setPolygonList, currentPolygon, setCurrentPolygon}) => {

  const [isOpenDialog, setIsOpenDialog] = useState(false)
  const polygonOptions = [
    {
      key: uuid(),
      id: 'btnCreatePolygon',
      toolTip: "Crear poligono",
      icon: <AddIcon />,
      color: 'var(--main-bg-color3)',
    },
    {
      key: uuid(),
      id: 'btnSaveCurrentPolygon',
      toolTip: "Guardar poligono",
      icon: <SaveIcon />,
      color: 'var(--main-bg-color3)',
    },
    {
      key: uuid(),
      id: 'btnDeletePolygon',
      toolTip: "Borrar poligonos",
      icon: <DeleteIcon />,
      color: 'var(--main-bg-color3)',
    },
    {
      key: uuid(),
      id: 'btnCancel',
      toolTip: "Cancelar",
      icon: <ClearIcon />,
      color: 'var(--main-bg-color5)',
    }
  ]

  const optionMenu = [
    {
      key: uuid(),
      id: 'btnAddMarker',
      toolTip: "Agregar marcador",
      icon: <AddLocation />,
      active: true,
    },
    {
      key: uuid(),
      id: 'btnAddPolygon',
      toolTip: "Agregar poligono",
      icon: <GridOn />,
      active: false,
    }
  ]

  const fnOpenDialog = () => setIsOpenDialog(!isOpenDialog)

  const fnOnClickDialog = (id) => {
    switch (id) {
      case 'btnAddMarker':
        fnOpenDialog()
        setToggleMarker(true)
        setTogglePolygon(false)
        setPolygonMode(OFF)
        break
      case 'btnAddPolygon':
        setTogglePolygon(!togglePolygon)
        setToggleMarker(false)
        break
      case 'btnCreatePolygon':
        fnOpenDialog()
        setPolygonMode(CREATE)
        break
      case 'btnDeletePolygon':
        (polygonList.length > 0) && setPolygonMode(DELETE)
        break
      case 'btnCancel':
        if (polygonMode === CREATE) {
          setPolygonMode(OFF)
          setCurrentPolygon(polygonModel())
          break
        }
        setPolygonList(polygonList.map(x => {
          return {
            ...x,
            isSelected: false
          }
        }))
        setCurrentPolygon(polygonModel())
        setPolygonMode(OFF)
        setTogglePolygon(false)
        setToggleMarker(true)
        break
      case 'btnSaveCurrentPolygon':
        if (fnIsExactSamePolygon(currentPolygon, polygonList)) {
          setCurrentPolygon(polygonModel())
          setPolygonMode(OFF)
        }
        setPolygonMode(OFF)
        setPolygonList(polygonList.concat(currentPolygon))
        setCurrentPolygon(polygonModel())
        break
      default:
        break
    }
  }

  const fnSelectNumericIcon = (id) => {
    try {
      switch (id) {
        case 1:
          return <Filter1Icon />
        case 2:
          return <Filter2Icon />
        case 3:
          return <Filter3Icon />
        case 4:
          return <Filter4Icon />
        case 5:
          return <Filter5Icon />
        case 6:
          return <Filter6Icon />
        case 7:
          return <Filter7Icon />
        case 8:
          return <Filter8Icon />
        case 9:
          return <Filter9Icon />
        default:
          break
      }
    } catch (error) {
      console.error('fnSelectNumericIcon => ', error)
    }
  }

  const polygonArrayBtn = (list) => {
    try {
      let newArray = [
        {
          key: 'cancelar',
          id: 999,
          icon: <ClearIcon />,
          toolTip: `Cancelar`,
          color: 'var(--main-bg-color5)',
          isSelected: false
        }
      ]
      newArray = newArray.concat(list.map((x, i) => {
        return {
          key: x.uniqueKey,
          id: i + 1,
          icon: (x.isSelected === true) ? <DoneIcon /> : fnSelectNumericIcon(i + 1),
          toolTip: `Eliminar poligono ${i + 1}`,
          color: 'var(--main-bg-color3)',
          isSelected: x.isSelected
        }
      }))
      return newArray
    } catch (error) {
      console.error('polygonArrayBtn => ', error)
    }
  }

  const fnDeletePolygon = (options) => {
    try {
      if (options.isSelected === true) {
        return setPolygonList(polygonList.filter(x => x.uniqueKey !== options.key))
      } else if (options.key === 'cancelar') {
        return setPolygonMode(OFF)
      }
      setPolygonList(polygonList.map((x, i) => {
        if (x.uniqueKey === options.key) {
          return {
            ...x,
            isSelected: !x.isSelected,
          }
        }
        return x
      }))
    } catch (error) {
      console.error('fnDeletePolygon => ', error)
    }
  }

  return (
    <div style={{ position: 'relative', paddingLeft: '20px', paddingTop: '10px', width: '40px', zIndex: 1004, height: '0px' }}>
      <SpeedDial
        ariaLabel="mapControlPanel"
        id="mapControlPanel"
        icon={<SpeedDialIcon />}
        onClose={fnOpenDialog}
        onOpen={fnOpenDialog}
        FabProps={{
          size: 'small',
          style: { backgroundColor: 'var(--main-bg-color2)' }
        }}
        open={isOpenDialog}
        direction={'down'}>
        {(togglePolygon === false) && optionMenu.map((options) => (
          <SpeedDialAction
            key={options.key}
            icon={options.icon}
            tooltipTitle={options.toolTip}
            onClick={() => fnOnClickDialog(options.id)}
            FabProps={
              {
                style: {
                  backgroundColor: options.active === true ? 'var(--main-bg-color4)' : 'var(--main-bg-color3)',
                  color: "white"
                },
              }
            }
          />
        ))}
        {(togglePolygon === true && polygonMode !== DELETE) && polygonOptions
          .filter(x => (polygonMode === CREATE)
            ? (currentPolygon.coords.length === 0)
              ? x.id === 'btnCancel'
              : x.id !== 'btnCreatePolygon' && x.id !== 'btnDeletePolygon'
            : x.id !== 'btnSaveCurrentPolygon')
          .map((options) => (
            <SpeedDialAction
              key={options.key}
              icon={options.icon}
              tooltipTitle={options.toolTip}
              onClick={() => fnOnClickDialog(options.id)}
              FabProps={
                {
                  style: {
                    backgroundColor: options.color,
                    color: "white"
                  },
                }
              }
            />
          ))}
        {(togglePolygon === true && polygonMode === DELETE) && polygonArrayBtn(polygonList).map((options) => (
          <SpeedDialAction
            key={options.key}
            icon={options.icon}
            tooltipTitle={options.toolTip}
            onClick={() => fnDeletePolygon(options)}
            FabProps={
              {
                style: {
                  backgroundColor: options.color,
                  color: "white"
                },
              }
            }
          />
        ))}
      </SpeedDial>
    </div>
  )
})

export default MapControlsLayer
