import { Grid, List, ListItem } from "@material-ui/core"
import TaPaper from "components/paper/TaPaper"
import TaTextField from "components/textfield/TaTextField"
import TaTypography from "components/typography/TaTypography"
import { memo } from "react"
import menuItemStyle from "components/styles/MenuItem"

const TaGeocoder = memo(({ resultList, country = 'VE', canShowList,
  fnGeocode, fnOnClickResult }) => {

  const classes = menuItemStyle()

  return (
    <div id={'searchBar'} style={{ position: 'relative', paddingLeft: '20px', paddingTop: '10px', zIndex: 1004, height: '0px' }}>
      <TaPaper
        backgroundColor={'var(--main-bg-color0)'}
        position={'relative'}
        width={'30%'}
        zIndex={1004}
        elevation={0}
        Content={
          <Grid container >
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
              <TaTextField
                name={'search'}
                fnOnEnter={fnGeocode}
                fullWidth={true}
                placeholder={'Buscar...'} />
            </Grid>
            {(canShowList) && <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
              <TaPaper
                elevation={0}
                maxHeight={'200px'}
                Content={
                  <List component="nav">
                    {resultList.map(result => {
                      const { placeId, display_name, lat, lon } = result
                      return (<ListItem key={placeId} className={classes.root} onClick={() => fnOnClickResult({ lat: parseFloat(lat), lon: parseFloat(lon) })}>
                        <TaTypography color={'var(--main-text-color0)'} text={display_name} />
                      </ListItem>)
                    }
                    )}
                  </List>
                }
              />
            </Grid>}
          </Grid>
        }
      />
    </div>
  )
})

export default TaGeocoder
