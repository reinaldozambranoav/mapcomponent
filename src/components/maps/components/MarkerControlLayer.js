import React, { memo, useEffect } from 'react'
import { useMapEvents } from 'react-leaflet'
import { fnIsControls } from '../functions'
import TaMarker from './TaMarker'

const MarkerControlLayer = memo(({ location, locations, toggleMarker, markerContent,
  disabled, fnAddMarker, fnDeleteMarker, coordinates, geocoder }) => {

  const mapEvents = useMapEvents({
    click(e) {
      try {
        if (e.originalEvent.pointerType === 'touch') {
          if (!fnIsControls(e)) {
            if (!disabled) {
              if (toggleMarker) {
                if (!geocoder) {
                  return fnAddMarker(e.latlng, false)
                }
              }
            }
          }
        } else {
          if (!fnIsControls(e)) {
            if (!disabled) {
              if (toggleMarker) {
                if (!geocoder) {
                  return fnAddMarker(e.latlng, false)
                }
              }
            }
          }
        }
      } catch (error) {
        console.error('Evento clic de react-leaflet => ', error)
      }
    }
  })

  useEffect(() => (coordinates !== undefined) && mapEvents.flyTo({ lat: coordinates.lat, lng: coordinates.lon }, 17), [coordinates])

  return (
    locations.map(x => (<TaMarker
      key={x.uniqueKey}
      locationData={x}
      markerContent={markerContent}
      fnDeleteMarker={fnDeleteMarker}
    />))
  )
})

export default MarkerControlLayer
