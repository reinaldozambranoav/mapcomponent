import React, { useEffect, useState } from 'react'
import { memo } from 'react'
import { Polygon, useMapEvents } from 'react-leaflet'
import { v4 as uuid } from 'uuid'
import '../../../index.css'
import { fnIsControls } from '../functions'
import { DELETE, OFF, CREATE } from '../MapConstants'
import TaMarker from './TaMarker'

const PolygonControlLayer = memo(({ polygonList, disabled, currentPolygon, setCurrentPolygon,
  polygonMode, }) => {

  const [polygonColor, setPolygonColor] = useState({ color: 'var(--main-bg-color2)' })

  const mapEvents = useMapEvents({
    click(e) {
      try {
        if (e.originalEvent.pointerType === 'touch') {
          if (fnIsControls(e)) {
            // SE QUEDA VACIO PARA QUE NO REACCIONE AL MAPA, SI NO A LOS CONTROLES EN SI
          } else if (!fnIsControls(e)) {
            // ACA DEBO COLOCAR LA LOGICA DE COLOCAR LOS MARCADORES QUE INDICAN LOS PUNTOS DE UNION DEL POLIGONO
            if (disabled) {
              if (polygonMode === CREATE) {
                setCurrentPolygon({
                  ...currentPolygon,
                  coords: currentPolygon.coords.concat({ uniqueKey: uuid(), lat: e.latlng.lat, lng: e.latlng.lng })
                })
              }
            }
          }
        }
        if (fnIsControls(e)) {
          // ACA ME COMPORTO CUANDO ESTOY INTERACTUANDO CON LOS CONTROLES DEL MAPA
        } else if (!fnIsControls(e)) {
          // ACA DEBO COLOCAR LA LOGICA DE COLOCAR LOS MARCADORES QUE INDICAN LOS PUNTOS DE UNION DEL POLIGONO
          if (disabled) {
            if (polygonMode === CREATE) {
              setCurrentPolygon({
                ...currentPolygon,
                coords: currentPolygon.coords.concat({ uniqueKey: uuid(), lat: e.latlng.lat, lng: e.latlng.lng })
              })
            }
          }
        }
      } catch (error) {
        console.error('Evento clic de react-leaflet => ', error)
      }
    },
  })

  useEffect(() => {
    switch (polygonMode) {
      case CREATE:
        return setPolygonColor({ color: 'var(--main-bg-color4)' })
      case DELETE:
        return setPolygonColor({ color: 'var(--main-bg-color1)' })
      case OFF:
        return setPolygonColor({ color: 'var(--main-bg-color3)' })
      default:
        break;
    }
  }, [polygonMode])

  const fnDeleteMarker = (data) => setCurrentPolygon(currentPolygon.filter(x => x.uniqueKey !== data))

  return (
    (polygonMode === CREATE)
      ? [
        currentPolygon.coords.map(x => (<TaMarker
          key={x.uniqueKey}
          forPolygon={true}
          locationData={x}
          coords={{ lat: x.lat, lng: x.lng }}
          fnDeleteMarker={fnDeleteMarker} />)),
        polygonList.map(x => (<Polygon
          pathOptions={polygonColor}
          positions={x.coords} />))
      ]
      : polygonList.map(x => (<Polygon
        key={x.uniqueKey}
        pathOptions={(x.isSelected === true) ? { color: 'var(--main-bg-color5)' } : polygonColor}
        positions={x.coords}
      />))
  )
})

export default PolygonControlLayer