import React, { memo } from 'react'
import { Marker, Popup } from 'react-leaflet'
import PropTypes from 'prop-types';

const TaMarker = memo(({ locationData, fnDeleteMarker, markerContent, coords, forPolygon }) => {

  return (
    <Marker key={locationData.uniqueKey} position={(coords) ? coords : locationData.coords}>
      {(forPolygon === false) && <Popup>
        {(markerContent !== undefined) && markerContent}
      </Popup>}
    </Marker >
  )
})

TaMarker.propTypes = {
  locationData: PropTypes.object.isRequired,
  fnDeleteMarker: PropTypes.func,
  coords: PropTypes.object,
  forPolygon: PropTypes.bool
}

TaMarker.defaultProps = {
  forPolygon: false
}

export default TaMarker
