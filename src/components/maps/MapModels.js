import { v4 as uuid } from 'uuid'

export const MarkerModel = (index, data) => {
  try {
    return {
      ...data,
      uniqueKey: uuid(),
      id: index,
    }
  } catch (error) {
    console.error('MarkerModel => ', error)
  }
}

export const LocationModel = (data, coordinates) => {
  try {
    return {
      coords: {
        lat: coordinates?.lat,
        lon: coordinates?.lon
      },
      country: data?.address?.country ?? '',
      countryCode: (data?.address?.country_code === undefined) ? '' : String(data.address.country_code).toUpperCase(),
      municipality: data?.address?.county ?? '',
      parish: data?.address?.municipality ?? '',
      state: data?.address?.state ?? '',
      postCode: data?.address?.postcode ?? '',
      road: data?.address?.road ?? '',
      suburb: data?.address?.suburb ?? '',
    }
  } catch (error) {
    console.error('LocationModel => ', error)
  }
}

export const polygonModel = () => {
  return {
    uniqueKey: uuid(),
    isSelected: false,
    coords: [],
  }
}